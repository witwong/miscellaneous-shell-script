#!/bin/bash
#To make it secure please use xargs to avoid showing token in terminal history by 
#for example: #echo $(cat token.txt) $(cat message.txt) | xargs ./check-cassandra-line-notify.sh
toks=$1
message=$2
if (systemctl -q is-active cassandra.service)
 then
 echo "Cassandra is running"
else
 echo "Cassandra is down"
 curl -X POST -H 'Authorization: Bearer '$toks'' -F "message=$message" https://notify-api.line.me/api/notify

fi
