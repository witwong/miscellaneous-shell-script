#!/bin/bash
/root/cassandra/bin/cqlsh 10.1.1.1 -f /root/export_cassandra_table_to_csv.cql
# Cassandra directory is mockup and so as cqlsh is need to be implement support of Cassandra environment with password later.
mv /root/backup_dir/$output_filename.csv /root/backup_dir/$(date '+%Y%m%d%H%M').csv
# Change the name of output file from cql script to be date format, So the version of each day could be track.